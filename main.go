package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

type config struct {
	appPort              string
	webhookURL           string
	targetServerHostname string
	targetServerPort     int
}

func main() {
	cfg, err := parseConfig()
	if err != nil {
		panic(err)
	}

	router := http.NewServeMux()
	router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("OK"))
	})

	go monitorServer(cfg)

	log.Printf("running HTTP server on %s", cfg.appPort)
	log.Fatal(http.ListenAndServe(cfg.appPort, router))
}

func monitorServer(cfg config) {
	log.Printf("sauerdaddy is watching %s:%d ...", cfg.targetServerHostname, cfg.targetServerPort)

	hook := NewSlackWebhook(cfg.webhookURL, 5*time.Second)
	server, err := NewSauerbratenServer(cfg.targetServerHostname, cfg.targetServerPort, 5*time.Second)
	if err != nil {
		panic(err)
	}

	somebodyIsPlaying := false
	for {
		time.Sleep(30 * time.Second)
		info, err := server.BasicInfo()
		if err != nil {
			log.Println(err)
			continue
		}

		if info.NumberOfClients == 0 {
			somebodyIsPlaying = false
			continue
		}

		if somebodyIsPlaying {
			continue
		}

		somebodyIsPlaying = true
		err = sendSlackNotification(hook, info.NumberOfClients, cfg.targetServerHostname)
		if err != nil {
			log.Println(err)
		}
	}
}

func sendSlackNotification(hook SlackWebhook, playerCount int, host string) error {
	prefix := "Hört hört, ihr Narren!"
	message := fmt.Sprintf("Ein einsamer Goblin sucht Freunde @ %s", host)
	if playerCount > 1 {
		message = fmt.Sprintf("%d attraktive Nerds aus deiner Umgebung befummeln sich @ %s", playerCount, host)
	}

	return hook.PostMessage(fmt.Sprintf(":direkt-feedback: %s %s :direkt-feedback:", prefix, message))
}

func parseConfig() (config, error) {
	var cfg config

	apiPort := os.Getenv("PORT")
	if apiPort == "" {
		return cfg, fmt.Errorf("missing env variable: PORT")
	}
	cfg.appPort = fmt.Sprintf(":%s", apiPort)

	cfg.webhookURL = os.Getenv("SLACK_WEBHOOK_URL")
	if cfg.webhookURL == "" {
		return cfg, fmt.Errorf("missing env variable: SLACK_WEBHOOK_URL")
	}

	cfg.targetServerHostname = os.Getenv("SAUERBRATEN_HOST")
	if cfg.targetServerHostname == "" {
		return cfg, fmt.Errorf("missing env variable: SAUERBRATEN_HOST")
	}

	port, err := intEnv("SAUERBRATEN_PORT")
	if err != nil {
		return cfg, err
	}
	cfg.targetServerPort = port

	return cfg, nil
}

func intEnv(name string) (int, error) {
	env := os.Getenv(name)
	if env == "" {
		return 0, fmt.Errorf("missing env variable: %s", name)
	}

	val, err := strconv.Atoi(env)
	if err != nil {
		return 0, fmt.Errorf("invalid integer value: %v", env)
	}

	return val, nil
}
