# sauerdaddy

Periodically checks for players on a Cube 2: Sauerbraten server and notifies a Slack channel using Slack Webhooks.

## How to run

Make sure to properly export the following environment variables:

```shell
PORT
SLACK_WEBHOOK_URL
SAUERBRATEN_HOST
SAUERBRATEN_PORT
```

## Routes

This app provides a single HTTP endpoint for health- or readiness checks:

```shell
curl -i localhost:12345/healthz
HTTP/1.1 200 OK
Connection: keep-alive
Date: Mon, 28 Feb 2022 19:55:03 GMT
Content-Length: 2
Content-Type: text/plain; charset=utf-8

OK
```