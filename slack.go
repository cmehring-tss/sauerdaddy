package main

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"time"
)

type SlackWebhook struct {
	url     string
	timeout time.Duration
}

func NewSlackWebhook(url string, timeout time.Duration) SlackWebhook {
	return SlackWebhook{url: url, timeout: timeout}
}

func (w SlackWebhook) PostMessage(message string) error {
	body, err := json.Marshal(postMessageRequest{Text: message})
	if err != nil {
		return err
	}

	request, err := http.NewRequest(http.MethodPost, w.url, bytes.NewReader(body))
	if err != nil {
		return err
	}

	client := &http.Client{Timeout: w.timeout}
	resp, err := client.Do(request)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	_, _ = io.Copy(io.Discard, resp.Body)

	return nil
}

type postMessageRequest struct {
	Text string `json:"text"`
}
