module gitlab.com/cmehring-tss/sauerdaddy

// +heroku goVersion go1.17
go 1.17

require (
	github.com/sauerbraten/cubecode v0.0.0-20191118162217-05ee938b0ef7 // indirect
	github.com/sauerbraten/extinfo v0.0.0-20191118162349-0387c695e739 // indirect
)
