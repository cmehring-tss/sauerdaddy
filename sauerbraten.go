package main

import (
	"github.com/sauerbraten/extinfo"
	"net"
	"time"
)

type SauerbratenServer struct {
	server *extinfo.Server
}

func NewSauerbratenServer(host string, port int, timeout time.Duration) (SauerbratenServer, error) {
	ips, err := net.LookupIP(host)
	if err != nil {
		return SauerbratenServer{}, err
	}

	udp := net.UDPAddr{IP: ips[0], Port: port}
	srv, err := extinfo.NewServer(udp, timeout)
	if err != nil {
		return SauerbratenServer{}, err
	}

	return SauerbratenServer{server: srv}, nil
}

func (s SauerbratenServer) BasicInfo() (extinfo.BasicInfo, error) {
	return s.server.GetBasicInfo()
}
